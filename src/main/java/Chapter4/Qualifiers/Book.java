package Chapter4.Qualifiers;

public class Book {
    private String title;
    private String isbnNumber;

    public Book(String title, String isbnNumber) {
        this.title = title;
        this.isbnNumber = isbnNumber;
    }

    public String getBook() {
        return this.title+":"+this.isbnNumber;
    }
}
