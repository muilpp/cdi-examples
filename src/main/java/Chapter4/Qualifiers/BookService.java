package Chapter4.Qualifiers;

import Chapter4.Qualifiers.utils.Generator;

import javax.inject.Inject;

public class BookService {

    @Inject
    @Generator(numberOfDigits = Generator.NumberOfDigits.EIGHT, printed = true)
    private NumberGenerator numberGenerator;

    public Book createBook(String title) {
        return new Book(title, numberGenerator.generateNumber());
    }
}