package Chapter4.Qualifiers;

import Chapter4.Qualifiers.utils.Generator;

import java.util.Random;

@Generator(numberOfDigits = Generator.NumberOfDigits.THIRTEEN, printed = false)
public class IsbnGenerator implements NumberGenerator{

    public String generateNumber() {
        return "E-13-3842-" + Math.abs(new Random().nextInt());
    }
}
