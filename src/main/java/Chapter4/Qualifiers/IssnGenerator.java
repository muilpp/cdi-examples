package Chapter4.Qualifiers;

import Chapter4.Qualifiers.utils.Generator;

import java.util.Random;

@Generator(numberOfDigits = Generator.NumberOfDigits.EIGHT, printed = false)
public class IssnGenerator implements NumberGenerator{

    public String generateNumber() {
        return "E-8-3842-" + Math.abs(new Random().nextInt());
    }
}
