package Chapter4.Qualifiers;

public interface NumberGenerator {
    String generateNumber();
}
