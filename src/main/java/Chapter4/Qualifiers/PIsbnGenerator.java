package Chapter4.Qualifiers;

import Chapter4.Qualifiers.utils.Generator;

import java.util.Random;

import static Chapter4.Qualifiers.utils.Generator.NumberOfDigits.THIRTEEN;

@Generator(numberOfDigits = THIRTEEN, printed = true)
public class PIsbnGenerator implements NumberGenerator{

    public String generateNumber() {
        return "P-13-3842-" + Math.abs(new Random().nextInt());
    }
}
