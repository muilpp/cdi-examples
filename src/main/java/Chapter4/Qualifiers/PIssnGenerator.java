package Chapter4.Qualifiers;

import Chapter4.Qualifiers.utils.Generator;

import java.util.Random;

@Generator(numberOfDigits = Generator.NumberOfDigits.EIGHT, printed = true)
public class PIssnGenerator implements NumberGenerator{

    public String generateNumber() {
        return "P-8-3842-" + Math.abs(new Random().nextInt());
    }
}
