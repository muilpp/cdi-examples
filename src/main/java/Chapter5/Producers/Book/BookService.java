package Chapter5.Producers.Book;


import Chapter4.Qualifiers.utils.EightDigits;

import javax.inject.Inject;

public class BookService {

    @Inject
    private String prefix;

    @Inject
    @EightDigits
    private int random;

    @Inject
    private long postfix;

    public Book createBook(String title) {
        String number = prefix + "-" + random + "_" + postfix;
        return new Book(title, number);
    }
}