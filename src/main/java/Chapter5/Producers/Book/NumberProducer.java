package Chapter5.Producers.Book;

import Chapter4.Qualifiers.utils.EightDigits;
import Chapter4.Qualifiers.utils.ThirteenDigits;
import javax.enterprise.inject.Produces;
import java.util.Random;

public class NumberProducer {

    @Produces
    private String pre = "7";

    @Produces
    @EightDigits
    private int randomEight = Math.abs(new Random().nextInt());

    @Produces
    @ThirteenDigits
    private int randomThirteen = 2;

    @Produces
    private long postfix() {
        return Math.abs(new Random().nextLong());
    }
}
