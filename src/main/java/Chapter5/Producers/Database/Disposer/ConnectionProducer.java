package Chapter5.Producers.Database.Disposer;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProducer {

    @Produces
    private Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/news","marc","Gaikukitxe_23");

//            Statement stmt= con.createStatement();
//            ResultSet rs=stmt.executeQuery("select * from emp");
//            while(rs.next())
//                System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
        return con;
    }

    private void closeConnection(@Disposes Connection con) throws SQLException {
        System.out.println("Closing connection...");
        con.close();
        System.out.println("Connection closed!");
    }
}
