package Chapter5.Producers.Database.Disposer;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionService {

    @Inject
    private Connection connection;

    public void doSomething() throws SQLException {
        System.out.println("Is connection closed? " + connection.isClosed());
    }
}
