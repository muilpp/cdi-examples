package Chapter5.Producers.Database.Disposer;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import java.sql.SQLException;

public class MainApp {
    public static void main(String[] args) throws SQLException {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        ConnectionService connectionService = container.instance().select(ConnectionService.class).get();
        connectionService.doSomething();
        weld.shutdown();
    }
}
