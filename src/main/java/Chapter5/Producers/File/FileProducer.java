package Chapter5.Producers.File;

import Chapter5.Producers.File.utils.Root;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileProducer {

    @Inject
    @Root
    private Path directory;

    @Produces
    public Path produceFile() throws IOException {

        System.out.println(directory);
        Path file = directory.resolve("fitxerDeProva.txt");

        if (Files.notExists(file))
            Files.createFile(file);

        return file;
    }
}
