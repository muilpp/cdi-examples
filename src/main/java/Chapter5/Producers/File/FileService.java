package Chapter5.Producers.File;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileService {

    @Inject
    private Path fileProducer;

    public void createFile(String textToWriteInFile) throws IOException {
        Files.write(fileProducer, textToWriteInFile.getBytes("utf-8"));
    }
}
