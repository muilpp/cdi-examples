package Chapter5.Producers.File;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import java.io.IOException;

public class MainApp {
    public static void main(String[] args) throws IOException {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        FileService fileService = container.instance().select(FileService.class).get();
        fileService.createFile("Test!");
        weld.shutdown();
    }
}
