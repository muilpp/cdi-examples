package Chapter5.Producers.File;

import Chapter5.Producers.File.utils.Root;
import Chapter5.Producers.File.utils.Tmp;

import javax.enterprise.inject.Produces;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathProducer {

    @Produces
    @Root
    private Path toolsRoot = Paths.get(System.getProperty("user.home"));

    @Produces
    @Tmp
    private Path tmpRoot = Paths.get("/tmp");

}
