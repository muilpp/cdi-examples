package Chapter5.Producers.Logger.InectionPoints;

import Chapter5.Producers.utils.Property;

import javax.inject.Inject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookService {

    @Inject
    @Property("book amb property")
    private Logger logger;

    public void printSomething() {
        logger.info(logger.getName() + " Print from BookService");
    }
}
