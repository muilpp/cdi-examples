package Chapter5.Producers.Logger.InectionPoints;


import Chapter5.Producers.utils.Property;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

public class LoggerProducer {

    @Produces
    @Property
    private Logger produceLogger(InjectionPoint ip) {
        System.out.println(ip.getAnnotated().getAnnotation(Property.class).value());
        return Logger.getLogger(ip.getMember().getDeclaringClass().getName());
    }
}
