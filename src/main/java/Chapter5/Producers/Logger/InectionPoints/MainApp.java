package Chapter5.Producers.Logger.InectionPoints;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class MainApp {
    public static void main(String[] args) {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        BookService bookService = container.instance().select(BookService.class).get();
        FileService fileService = container.instance().select(FileService.class).get();
        ItemService itemService = container.instance().select(ItemService.class).get();

        bookService.printSomething();
        fileService.printSomething();
        itemService.printSomething();
        weld.shutdown();
    }
}
