package Chapter6.CallBackAnnotations.Book;

public class Book {
    private String title;
    private Float price;
    private String isbnNumber;

    public Book(String title, Float price, String isbnNumber) {
        this.title = title;
        this.price = price;
        this.isbnNumber = isbnNumber;
    }

    public String getBook() {
        return this.title+":"+this.isbnNumber;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "title="+title+", " + " number="+isbnNumber+", " + " price="+price;
    }
}
