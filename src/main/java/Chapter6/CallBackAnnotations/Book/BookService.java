package Chapter6.CallBackAnnotations.Book;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import java.util.logging.Logger;

public class BookService {

    @Inject
    private NumberGenerator numberGenerator;

    @Inject
    private Logger logger;

    @PostConstruct
    private void setUp() {
        logger.info("Logging some random shit at initialization");
    }

    public Book createBook(String title, Float price) {
        return new Book(title, price, numberGenerator.generateNumber());
    }

    @PreDestroy
    private void tearDown() {
        logger.info("Logging some random shit on pre destroy");
    }
}