package Chapter6.CallBackAnnotations.Book;

public interface NumberGenerator {
    String generateNumber();
}
