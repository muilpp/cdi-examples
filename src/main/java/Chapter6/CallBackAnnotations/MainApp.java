package Chapter6.CallBackAnnotations;

import Chapter6.CallBackAnnotations.Book.Book;
import Chapter6.CallBackAnnotations.Book.BookService;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class MainApp {
    public static void main(String[] args) throws InterruptedException {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        BookService bookService = container.instance().select(BookService.class).get();
        Book book = bookService.createBook("Bones tardes amics meus tots", 10F);
        System.out.println(book.toString());
        weld.shutdown();
    }
}