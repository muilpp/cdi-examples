package Chapter6.Decorators;

import java.util.List;

public interface Computable {
    PurchaseOrder compute(List<Item> items);
    PurchaseOrder createPurchaseOrder(List<Item> items);
}
