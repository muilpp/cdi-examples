package Chapter6.Decorators;

public class Item {

    private int subTotal;

    public Item() {}

    public Item(int price) {
        this.subTotal = price;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }
}
