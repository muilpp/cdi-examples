package Chapter6.Decorators;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import java.util.Arrays;
import java.util.List;

public class MainApp {
    public static void main(String[] args) throws InterruptedException {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        PurchaseOrderService orderService = container.instance().select(PurchaseOrderService.class).get();
        List<Item> orderList = Arrays.asList(new Item(1), new Item(2), new Item(3), new Item(4));
        PurchaseOrder order = orderService.compute(orderList);
        System.out.println(order.toString());
        weld.shutdown();
    }
}