package Chapter6.Decorators;

public class PurchaseOrder {
    private int total, totalAfterDiscount;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalAfterDiscount() {
        return totalAfterDiscount;
    }

    public void setTotalAfterDiscount(int totalAfterDiscount) {
        this.totalAfterDiscount = totalAfterDiscount;
    }

    @Override
    public String toString() {
        return "Total -> " + total +  ", total after discount -> " + totalAfterDiscount;
    }
}
