package Chapter6.Decorators;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import java.util.List;

@Decorator
//The class is abstract so we are not forced to implement all methods of the interface we are implementing
public abstract class PurchaseOrderDecorator implements Computable {

    //Delegate here is used to point at the target class we are decorating
    @Inject @Delegate
    private Computable purchaseOrderService;

    public PurchaseOrder compute(List<Item> itemsList) {
        PurchaseOrder po = purchaseOrderService.compute(itemsList);
        po.setTotalAfterDiscount((int)(po.getTotal()*0.85));

        return po;
    }
}