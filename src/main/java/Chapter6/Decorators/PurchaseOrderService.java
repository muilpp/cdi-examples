package Chapter6.Decorators;

import javax.inject.Inject;
import java.util.List;

public class PurchaseOrderService implements Computable {
    @Inject
    private PurchaseOrder po;

    @Override
    public PurchaseOrder compute(List<Item> items) {

        int subTotal =  items.stream().mapToInt(i -> i.getSubTotal()).sum();
        po.setTotal(subTotal);
        po.setTotalAfterDiscount(subTotal);

        return po;
    }

    @Override
    public PurchaseOrder createPurchaseOrder(List<Item> items) {
        return compute(items);
    }
}
