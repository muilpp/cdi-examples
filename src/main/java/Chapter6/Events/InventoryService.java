package Chapter6.Events;

import Chapter6.Events.utils.SecondHand;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.logging.Logger;

public class InventoryService {

    @Inject
    private Logger logger;

    public void addItems(@Observes @SecondHand PurchaseOrder po) {
        logger.info("Adding items in Inventory Service");
    }
}
