package Chapter6.Events;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import java.util.Arrays;
import java.util.List;

//Events are synchronous, meaning the execution of the program won't continue until the events are executed
//They can also be qualified, as in this example
public class MainApp {
    public static void main(String[] args) throws InterruptedException {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        PurchaseOrderService orderService = container.instance().select(PurchaseOrderService.class).get();
        List<Item> orderList = Arrays.asList(new Item(1), new Item(2), new Item(3), new Item(4));
        PurchaseOrder order = orderService.create(orderList);
        System.out.println(order.toString());
        weld.shutdown();
    }
}