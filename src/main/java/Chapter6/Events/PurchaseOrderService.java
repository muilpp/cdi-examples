package Chapter6.Events;

import Chapter6.Events.utils.NewProduct;
import Chapter6.Events.utils.SecondHand;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.List;

public class PurchaseOrderService {
    @Inject @NewProduct
    private PurchaseOrder po;

    @Inject @NewProduct
    private Event<PurchaseOrder> purchaseNewProductOrderEvent;

    @Inject @SecondHand
    private Event<PurchaseOrder> purchaseSecondHandOrderEvent;

    public PurchaseOrder create(List<Item> items) {

        int subTotal =  items.stream().mapToInt(i -> i.getSubTotal()).sum();
        po.setTotal(subTotal);
        po.setTotalAfterDiscount(subTotal);

        purchaseNewProductOrderEvent.fire(po);

        return po;
    }
}