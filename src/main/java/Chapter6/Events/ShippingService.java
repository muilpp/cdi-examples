package Chapter6.Events;

import Chapter6.Events.utils.NewProduct;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.logging.Logger;

public class ShippingService {

    @Inject
    private Logger logger;

    public void shipItems(@Observes @NewProduct PurchaseOrder po) {
        logger.info("Shipping items in Shipping Service");
    }
}
