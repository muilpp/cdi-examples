package Chapter6.Events;

import Chapter6.Events.utils.NewProduct;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.logging.Logger;

public class StatisticsService {

    @Inject
    private Logger logger;

    public void itemsSold(@Observes @NewProduct PurchaseOrder po) {
        logger.info("Selling items in Statistics Service");
    }
}
