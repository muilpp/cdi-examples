package Chapter6.Interceptors.MethodInterceptors.Book;

import Chapter6.Interceptors.MethodInterceptors.Book.utils.Loggable;

import javax.inject.Inject;

@Loggable
public class BookService {

    @Inject
    private NumberGenerator numberGenerator;

    public Book createBook(String title, Float price) {
        return new Book(title, price, numberGenerator.generateNumber());
    }

    public void raisePrice(Book book) throws InterruptedException {
        Thread.sleep(1000);
        book.setPrice(book.getPrice()*2.5F);
        //return book;
    }
}