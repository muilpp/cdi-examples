package Chapter6.Interceptors.MethodInterceptors.Book;


import Chapter6.Interceptors.MethodInterceptors.Book.utils.GeneratorQualifier;
import java.util.Random;

public class IsbnGenerator implements NumberGenerator {

    public String generateNumber() {
        return "E-13-3842-" + Math.abs(new Random().nextInt());
    }
}
