package Chapter6.Interceptors.MethodInterceptors.Book;

public interface NumberGenerator {
    String generateNumber();
}
